import {useEffect} from 'react';

export const useDisableMainPageScroll = () => {
    useEffect(()=>{
        document.body.style.overflow = 'hidden';
        return ()=>document.body.style.overflow = 'unset';
    },[]);
}

  