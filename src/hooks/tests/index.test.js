import React from 'react';
import {useDisableMainPageScroll} from '..';
import {mount} from 'enzyme';
import {HookWrapperComponent} from '../../mock'

describe('useDisableMainPageScroll test', ()=>{
    it('set properties to document.body', ()=>{
        const wrapper = mount(<HookWrapperComponent hook={useDisableMainPageScroll}/>);
        expect(document.body.style).toHaveProperty('overflow', 'hidden');
        wrapper.unmount();
        expect(document.body.style).toHaveProperty('overflow', 'unset');
        
    });
});