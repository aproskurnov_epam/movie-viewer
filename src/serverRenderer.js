import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter, matchPath } from 'react-router-dom';
import {App} from './components/app';
import {configureStore} from './store';
import routes from './routes';

function renderHTML(html, preloadedState) {
  return `
      <!doctype html>
      <html>
        <head>
          <meta charset=utf-8>
          <title>Movie viewer</title>
          ${process.env.NODE_ENV === 'development' ? '' : '<link href="/css/main.css" rel="stylesheet" type="text/css">'}
        </head>
        <body>
          <div id="app">${html}</div>
          <div id="popup"></div>
          <script>
            // WARNING: See the following for security issues around embedding JSON in HTML:
            // http://redux.js.org/docs/recipes/ServerRendering.html#security-considerations
            window.PRELOADED_STATE = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
          </script>
          <script src="/js/main.js"></script>
        </body>
      </html>
  `;
}

export default function serverRenderer() {
  return (req, res) => {
    const store = configureStore();
    const promises = routes.reduce((acc, route) => {
      const match = matchPath(req.url, route);
      if (match && route.component && route.initialAction) {
        acc.push(Promise.resolve(store.dispatch(route.initialAction(match.params))));
      }
      return acc;
    }, []);

    console.log('p', promises);
    Promise.all(promises).then(() => {
      // This context object contains the results of the render
      const context = {};

      const renderRoot = () => (
        <App
          context={context}
          location={req.url}
          Router={StaticRouter}
          store={store}
        />
      );

      

      renderToString(renderRoot());

      // context.url will contain the URL to redirect to if a <Redirect> was used
      if (context.url) {
        res.writeHead(302, {
          Location: context.url,
        });
        res.end();
        return;
      }

      const htmlString = renderToString(renderRoot());
      const preloadedState = store.getState();

      console.log('4324324', htmlString);

      res.send(renderHTML(htmlString, preloadedState));
    }).catch(e => console.error(e));
  };
}
