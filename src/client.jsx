import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { App } from './components/app';
import {BrowserRouter} from 'react-router-dom';
import {configureStore} from './store';

const store = configureStore();
ReactDOM.hydrate(<App Router={BrowserRouter} store={store}/>, document.getElementById('app'));