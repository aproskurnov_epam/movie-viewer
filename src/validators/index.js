export const reqired = (value)=>{
    if (!value || (Array.isArray(value) && !value.length)){
        return 'Required';
    }
}
export const min = (value, min)=>{
    if (value < min){
        return `min ${min}`;
    }
}

export const max = (value, max)=>{
    if (value > max){
        return `max ${max}`;
    }
}

export const movieFormValidator = values => {
    const errors = {};

    const titleError = reqired(values.title);
    const posterPathError = reqired(values.poster_path);
    const overviewError = reqired(values.overview);
    const genresError = reqired(values.genres);
    const runtimeError = reqired(values.runtime) || min(values.runtime, 0)

    if (titleError){
        errors.title = titleError;
    }
    
    if (posterPathError){
        errors.poster_path = posterPathError;
    }

    if (overviewError){
        errors.overview = overviewError;
    }

    if (genresError){
        errors.genres = genresError;
    }

    if (runtimeError){
        errors.runtime = runtimeError;
    }

    return errors;
}