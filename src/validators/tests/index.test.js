import {
 reqired,
 min,
 max,
 movieFormValidator} from '..';

describe('validators tests', ()=>{

    it('reqired empty string', ()=>{
        const actualResult = reqired('');
        const expectedResult = 'Required';
        expect(actualResult).toEqual(expectedResult);
    });

    it('reqired empty array', ()=>{
        const actualResult = reqired([]);
        const expectedResult = 'Required';
        expect(actualResult).toEqual(expectedResult);
    });

    it('< min', ()=>{
        const actualResult = min(9, 10);
        const expectedResult = 'min 10';
        expect(actualResult).toEqual(expectedResult);
    });

    it('> max', ()=>{
        const actualResult = max(11, 10);
        const expectedResult = 'max 10';
        expect(actualResult).toEqual(expectedResult);
    });

    it('< max', ()=>{
        const actualResult = max(9, 10);
        const expectedResult = undefined;
        expect(actualResult).toEqual(expectedResult);
    });
   
    it('movieFormValidator without errors', ()=>{
        const data = {title: 'mock', poster_path: 'mock', overview: 'mock', genres: 'mock', runtime: 10};
        const actualResult = movieFormValidator(data);
        const expectedResult = {};
        expect(actualResult).toEqual(expectedResult);
    });

    it('movieFormValidator with errors', ()=>{
        const data = {};
        const actualResult = movieFormValidator(data);
        const expectedResult = {title: 'Required', poster_path: 'Required', overview: 'Required', genres: 'Required', runtime: 'Required'};
        expect(actualResult).toEqual(expectedResult);
    });

});
            