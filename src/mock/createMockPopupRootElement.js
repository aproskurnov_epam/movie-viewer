export const createMockPopupRootElement = () => {
    const el = document.createElement('div');
    el.setAttribute('id', 'popup');
    document.body.appendChild(el);
}

