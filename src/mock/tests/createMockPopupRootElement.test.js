import {createMockPopupRootElement} from '../createMockPopupRootElement';


describe('createMockPopupRootElement test', ()=>{
    it('call it', ()=>{
        createMockPopupRootElement();
        const popup = document.getElementById('popup');
        
        expect(popup).not.toBeNull();
    });
});