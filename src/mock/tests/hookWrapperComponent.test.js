import React from 'react';
import {mount} from 'enzyme';
import {HookWrapperComponent} from '../../mock/hookWrapperComponent'

describe('HookWrapperComponent test', ()=>{
    it('call hook inside wrapper component', ()=>{
        const hookMock = jest.fn();
        mount(<HookWrapperComponent hook={hookMock}/>);
        expect(hookMock).toBeCalled();
    });
});