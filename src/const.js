import {releaseSort, voteAverageSort} from './helpers';

export const GENRES = Object.freeze({
    All:'All',
    Documentary :'Documentary',
    Comedy:'Comedy',
    Horror: 'Horror',
    Crime: 'Crime'
});

export const SORT = Object.freeze({
    ReleaseDate: 'release_date',
    Rating: 'vote_average'
});

export const SORT_OPTIONS = [
    {
        value: SORT.ReleaseDate,
        label: 'Release Date'
    },
    {
        value: SORT.Rating,
        label: 'Rating'
    },
]

export const GENRE_OPTIONS = [
    {
        value: 'All',
        label: 'All'
    },
    {
        value: 'Documentary',
        label: 'Documentary'
    },
    {
        value: 'Comedy',
        label: 'Comedy'
    },
    {
        value: 'Horror',
        label: 'Horror'
    },
    {
        value: 'Crime',
        label: 'Crime'
    },
]

export const SORT_FUNCTION = {
    release_date: releaseSort, 
    vote_average: voteAverageSort
}

export const BUTTON_APPEARANCE = Object.freeze({
    Default: 'default',
    Transparent: 'transparent'
});

export const BUTTON_TYPES = Object.freeze({
    Button: 'button',
    Submit: 'submit'
});