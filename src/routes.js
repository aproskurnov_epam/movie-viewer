import Home from './pages/home';
import Film from './pages/film';
import Search from './pages/search';
import PageNotFound from './pages/not-found';

const routes = [
  {
    path: '/',
    component: Home.component,
    initialAction: Home.initialAction,
    exact: true
  },
  {
    path: '/film/:idFilm',
    component: Film.component,
    initialAction: Film.initialAction,
    exact: true
  },
  {
    path: '/search/:searchQuery',
    component: Search.component,
    initialAction: Search.initialAction,
    exact: true
  },
  {
    path: '*',
    component: PageNotFound.component,
    initialAction: PageNotFound.initialAction,
  },
];

export default routes;
