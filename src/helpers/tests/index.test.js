
import {setFormikField, releaseSort, voteAverageSort, prepareGenresToDropdown, prepareGenresToForm} from '..';
import {GENRES} from '../../const';

describe('helpers test', ()=>{

    describe('setFormikField test', ()=>{
        it('call callback with appropriate args', ()=>{
            const mockCallback = jest.fn();
            const mockName = 'mockName';
            const mockValue = 'mockValue';
            setFormikField(mockCallback, {name: mockName, value: mockValue});
            expect(mockCallback).toBeCalledWith(mockName, mockValue);
        });        
    });

    it('releaseSort', ()=>{
        const expectedResult = -1;
        const actualResult = releaseSort({release_date: 1}, {release_date: 2});
        expect(actualResult).toBe(expectedResult);
    });   
    
    it('voteAverageSort', ()=>{
        const expectedResult = -1;
        const actualResult = voteAverageSort({vote_average: 'a'}, {vote_average: 'b'});
        expect(actualResult).toBe(expectedResult);
    });

    it('prepareGenresToDropdown array', ()=>{
        const expectedResult = GENRES.Documentary;
        const actualResult = prepareGenresToDropdown([GENRES.Documentary]);
        expect(actualResult).toBe(expectedResult);
    });

    it('prepareGenresToDropdown single', ()=>{
        const expectedResult = GENRES.Documentary;
        const actualResult = prepareGenresToDropdown(GENRES.Documentary);
        expect(actualResult).toBe(expectedResult);
    });

    it('prepareGenresToForm', ()=>{
        const expectedResult = {name: 'name', value: [GENRES.Documentary]}
        const actualResult = prepareGenresToForm({name: 'name', value: GENRES.Documentary});
        expect(actualResult).toEqual(expectedResult);
    });


});


