export const setFormikField = (callback, {name, value}) =>{
    callback(name, value);
}

export const releaseSort = (a, b)=>a.release_date-b.release_date;

export const voteAverageSort = (a,b)=>(a.vote_average).localeCompare(b.vote_average);

export const prepareGenresToDropdown = (genresValue) => {
    return genresValue && Array.isArray(genresValue) ? genresValue[0] : genresValue
}

export const prepareGenresToForm = ({name, value})=>{
    return {name, value: [value]};
}