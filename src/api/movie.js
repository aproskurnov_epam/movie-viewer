import {stringify} from 'query-string';
import fetch from 'node-fetch';

const host = 'http://localhost:4000/'
const searchBy = 'title';

export const EMPTY_LIST = {data: [],
limit: 10,
offset: 0,
totalAmount: 0}

export const getMovieList = async ({sort, filter, search}) =>{
    if (!search){
        return EMPTY_LIST;
    }
    const query = stringify({sortBy:sort, filter, search, searchBy}, {arrayFormat:'bracket'});
    const response = await fetch(`${host}movies?${query}`, {method:'GET'});
    return response.json();
}

export const createMovie = async (data) => {
    const params = {
        method:'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type' : 'application/json'
        }
    };
    const response = await fetch(`${host}movies`, params);
    return response.json();
}

export const updateMovie = async (data) => {
    const params = {
        method:'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-Type' : 'application/json'
        }
    };
    const response = await fetch(`${host}movies`, params);
    return response.json();
}

export const deleteMovie = async (id) => {
    const response = await fetch(`${host}movies/${id}`, {method:'DELETE'});
    return  Boolean(response.ok);
}

export const getMovie = async (id) => {
    const response = await fetch(`${host}movies/${id}`, {method:'GET'})
    return  response.ok? response.json() : false;
}