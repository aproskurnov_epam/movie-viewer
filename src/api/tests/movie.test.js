import {getMovieList, createMovie,updateMovie,deleteMovie,getMovie, EMPTY_LIST} from '../movie';
import {SORT, GENRES} from '../../const';

let mockedSuccessfulFetch;
let mockedWrongFetch;

const mockSuccessfulFetch = () =>{
    const successfulResponse = {json: ()=>'mockedResponseData', ok: true};
    mockedSuccessfulFetch = global.fetch = jest.fn().mockResolvedValue(successfulResponse);
}

const mockWrongFetch = () =>{
    const wrongResponse = {ok: false};
    mockedWrongFetch = global.fetch = jest.fn().mockResolvedValue(wrongResponse);
}

describe('API test', ()=>{
    beforeEach(()=>{
        mockSuccessfulFetch();
    });
    describe('getMovieList',()=>{
        it('when search is empty return empty list',async ()=>{
            const expectedResult = EMPTY_LIST;
            const actualResult = await getMovieList({sort:SORT.Rating, filter:GENRES.Documentary});
            expect(actualResult).toEqual(expectedResult);
        });

        it('when search is not empty return call fetch', ()=>{
            
        });

        it('when search is not empty return request response data', async()=>{
            const expectedRequestResult = 'http://localhost:4000/movies?filter=Documentary&search=searchString&searchBy=title&sortBy=vote_average';
            const expectedResponseResult = 'mockedResponseData';
            const actualResult = await getMovieList({sort:SORT.Rating, filter:GENRES.Documentary, search:"searchString"});
            expect(mockedSuccessfulFetch).toBeCalledWith(expectedRequestResult, expect.anything())
            expect(actualResult).toEqual(expectedResponseResult);
        });
    });

    describe('createMovie',()=>{
        it('returned mocked response', async()=>{
            const expectedRequestResult = 'http://localhost:4000/movies';
            const expectedResponseResult = 'mockedResponseData';
            const actualResult = await createMovie();
            expect(mockedSuccessfulFetch).toBeCalledWith(expectedRequestResult, expect.anything())
            expect(actualResult).toEqual(expectedResponseResult);
        });
    });

    describe('updateMovie',()=>{
        it('returned mocked response', async()=>{
            const expectedRequestResult = 'http://localhost:4000/movies';
            const expectedResponseResult = 'mockedResponseData';
            const actualResult = await updateMovie();
            expect(mockedSuccessfulFetch).toBeCalledWith(expectedRequestResult, expect.anything())
            expect(actualResult).toEqual(expectedResponseResult);
        });
    });

    describe('deleteMovie',()=>{
        it('call correct url', async()=>{
            const expectedRequestResult = 'http://localhost:4000/movies/1';
            await deleteMovie(1);
            expect(mockedSuccessfulFetch).toBeCalledWith(expectedRequestResult, expect.anything())
        });

        it('returned mocked response and return successful result', async()=>{
            const expectedResponseResult = true;
            const actualResult = await deleteMovie(1);
            expect(actualResult).toEqual(expectedResponseResult);
        });

        it('returned mocked response and return wrong result', async()=>{
            mockWrongFetch();
            const expectedResponseResult = false;
            const actualResult = await deleteMovie(1);
            expect(actualResult).toEqual(expectedResponseResult);
        });
    });

    describe('getMovie',()=>{
        it('call correct url', async()=>{
            const expectedRequestResult = 'http://localhost:4000/movies/1';
            await getMovie(1);
            expect(mockedSuccessfulFetch).toBeCalledWith(expectedRequestResult, expect.anything())
        });

        it('returned mocked response and return successful result', async()=>{
            const expectedResponseResult = 'mockedResponseData';
            const actualResult = await getMovie(1);
            expect(actualResult).toEqual(expectedResponseResult);
        });

        it('returned mocked response and return wrong result', async()=>{
            mockWrongFetch();
            const expectedResponseResult = false;
            const actualResult = await getMovie(1);
            expect(actualResult).toEqual(expectedResponseResult);
        });
    });

    
});







