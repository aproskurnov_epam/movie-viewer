import {
    getMovieList as getMovieListApi,
    createMovie as createMovieApi,
    updateMovie as updateMovieApi, 
    deleteMovie as deleteMovieApi, 
} from '../api';
import {ACTIONS} from './types';

export const getMovies = () => {
    return async (dispatch, getState) => {
        const {filter, sort, search} = getState();
        const {data : movies, totalAmount} = await getMovieListApi( {sort, filter, search} );
        dispatch({ type: ACTIONS.GET_MOVIE_LIST, payload: {movies, totalAmount}});
    };
  };

export const createMovie = (data) => {
    return async (dispatch, getState) => {
        await createMovieApi(data);
        const {filter, sort, search} = getState();
        const {data : movies, totalAmount} = await getMovieListApi( {sort, filter, search} );
        dispatch({ type: ACTIONS.GET_MOVIE_LIST, payload: {movies, totalAmount}});
    };
  };

export const updateMovie = (data) => {
    return async (dispatch, getState) => {
        await updateMovieApi(data);
        const {filter, sort, search} = getState();
        const {data : movies, totalAmount} = await getMovieListApi( {sort, filter, search} );
        dispatch({ type: ACTIONS.GET_MOVIE_LIST, payload: {movies, totalAmount}});
    };
};

export const deleteMovie = (id) => {
    return async (dispatch, getState) => {
        await deleteMovieApi(id);
        const {filter, sort, search} = getState();
        const {data : movies, totalAmount} = await getMovieListApi( {sort, filter, search} );
        dispatch({ type: ACTIONS.GET_MOVIE_LIST, payload: {movies, totalAmount}});
    };
};

export const changeMovieSorting = (sort) => {
    return async (dispatch, getState) => {
        const {filter, search} = getState();
        const {data : movies, totalAmount} = await getMovieListApi( {sort, filter, search} );
        dispatch({ type: ACTIONS.CHANGE_SORTING, payload: sort});
        dispatch({ type: ACTIONS.GET_MOVIE_LIST, payload: {movies, totalAmount}});
    };
};

export const changeMovieGenreFilter = (filter) => {
    return async (dispatch, getState) => {
        const {sort, search} = getState();
        const {data : movies, totalAmount} = await getMovieListApi( {sort, filter, search} );
        dispatch({ type: ACTIONS.CHANGE_GENRE_FILTER, payload: filter});
        dispatch({ type: ACTIONS.GET_MOVIE_LIST, payload: {movies, totalAmount}});
    };
};

export const changeMovieSearchQuery = (search) => {
    return async (dispatch, getState) => {
        const {filter, sort} = getState();
        const {data : movies, totalAmount} = await getMovieListApi( {sort, filter, search} );
        dispatch({ type: ACTIONS.CHANGE_SEARCH_QUERY, payload: search});
        dispatch({ type: ACTIONS.GET_MOVIE_LIST, payload: {movies, totalAmount}});
    };
};