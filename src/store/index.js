import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import {rootReducer} from './reducer';

export const configureStore =  (initialState) => {
  return createStore(rootReducer, initialState, applyMiddleware(thunkMiddleware));
};
