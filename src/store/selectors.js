export const moviesSelector = state => state.movies;
export const sortSelector = state => state.sort;
export const filterSelector = state => state.filter;
export const totalAmountSelector = state => state.totalAmount;