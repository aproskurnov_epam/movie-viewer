import {
 moviesSelector,
 sortSelector,
 filterSelector,
 totalAmountSelector
} from '../selectors';
import {initialState} from '../reducer';

describe('redux selectors test', ()=>{

    it('moviesSelector', ()=>{
        const actualResult = moviesSelector(initialState);
        const expectedResult = initialState.movies;
        expect(actualResult).toEqual(expectedResult);
    });

    it('sortSelector', ()=>{
        const actualResult = sortSelector(initialState);
        const expectedResult = initialState.sort;
        expect(actualResult).toEqual(expectedResult);
    });

    it('filterSelector', ()=>{
        const actualResult = filterSelector(initialState);
        const expectedResult = initialState.filter;
        expect(actualResult).toEqual(expectedResult);
    });

    it('totalAmountSelector', ()=>{
        const actualResult = totalAmountSelector(initialState);
        const expectedResult = initialState.totalAmount;
        expect(actualResult).toEqual(expectedResult);
    });

});