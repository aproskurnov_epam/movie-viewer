import {initialState, rootReducer} from '../reducer';
import {SORT, GENRES} from '../../const';
import {ACTIONS} from '../types';
   
describe('redux reducer test', ()=>{

    it('CHANGE_SORTING', ()=>{
        const actualResult = rootReducer(initialState, {type: ACTIONS.CHANGE_SORTING, payload: SORT.Rating});
        const expectedResult = {...initialState, sort: SORT.Rating};
        expect(actualResult).toEqual(expectedResult);
    });
    it('CHANGE_GENRE_FILTER', ()=>{
        const actualResult = rootReducer(initialState, {type: ACTIONS.CHANGE_GENRE_FILTER, payload: [GENRES.Documentary]});
        const expectedResult = {...initialState, filter: [GENRES.Documentary]};
        expect(actualResult).toEqual(expectedResult);
    });
    it('CHANGE_SEARCH_QUERY', ()=>{
        const actualResult = rootReducer(initialState, {type: ACTIONS.CHANGE_SEARCH_QUERY, payload: 'search string'});
        const expectedResult = {...initialState, search: 'search string'};
        expect(actualResult).toEqual(expectedResult);
    });
    it('GET_MOVIE_LIST', ()=>{
        const actualResult = rootReducer(initialState, {type: ACTIONS.GET_MOVIE_LIST, payload: {movies:[], totalAmount:0}});
        const expectedResult = {...initialState, movies: [], totalAmount: 0};
        expect(actualResult).toEqual(expectedResult);
    });

});
            