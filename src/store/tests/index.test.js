import { createStore } from "redux";
import {store} from '..';

jest.mock('redux', ()=>({createStore: jest.fn(), applyMiddleware: jest.fn()}));

describe('store creating', ()=>{
    it('createStore is called', ()=>{
        expect(createStore).toBeCalled();
    });
});