import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
    getMovieList as getMovieListApi
} from '../../api';
import {getMovies,
 createMovie,
 updateMovie,
 deleteMovie,
 changeMovieSorting,
 changeMovieGenreFilter,
 changeMovieSearchQuery} from '../actions';
import {ACTIONS} from '../types';
import {SORT, GENRES} from '../../const';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('../../api', ()=>({
    getMovieList: jest.fn(),
    createMovie: jest.fn(),
    updateMovie: jest.fn(),
    deleteMovie: jest.fn(),
}));

describe('redux actions test', ()=>{
    let store;
    beforeEach(()=>{
        store = mockStore();
    });

    it('getMovies action', async ()=>{
        getMovieListApi.mockResolvedValue({data:[], totalAmount:0});
        await store.dispatch(getMovies());
        const actualResult = store.getActions();
        const expectedResult = [{type: ACTIONS.GET_MOVIE_LIST, payload: {movies:[], totalAmount:0}}];
        expect(actualResult).toEqual(expectedResult);
    });

    it('createMovie action', async ()=>{
        getMovieListApi.mockResolvedValue({data:[], totalAmount:0});
        await store.dispatch(createMovie());
        const actualResult = store.getActions();
        const expectedResult = [{type: ACTIONS.GET_MOVIE_LIST, payload: {movies:[], totalAmount:0}}];
        expect(actualResult).toEqual(expectedResult);
    });

    it('updateMovie action', async ()=>{
        getMovieListApi.mockResolvedValue({data:[], totalAmount:0});
        await store.dispatch(updateMovie());
        const actualResult = store.getActions();
        const expectedResult = [{type: ACTIONS.GET_MOVIE_LIST, payload: {movies:[], totalAmount:0}}];
        expect(actualResult).toEqual(expectedResult);
    });

    it('deleteMovie action', async ()=>{
        getMovieListApi.mockResolvedValue({data:[], totalAmount:0});
        await store.dispatch(deleteMovie());
        const actualResult = store.getActions();
        const expectedResult = [{type: ACTIONS.GET_MOVIE_LIST, payload: {movies:[], totalAmount:0}}];
        expect(actualResult).toEqual(expectedResult);
    });

    it('changeMovieSorting action', async ()=>{
        getMovieListApi.mockResolvedValue({data:[], totalAmount:0});
        await store.dispatch(changeMovieSorting(SORT.ReleaseDate));
        const actualResult = store.getActions();
        const expectedResult = [
            {type: ACTIONS.CHANGE_SORTING, payload: SORT.ReleaseDate},
            {type: ACTIONS.GET_MOVIE_LIST, payload: {movies:[], totalAmount:0}}
        ]
        expect(actualResult).toEqual(expectedResult);
    });

    it('changeMovieGenreFilter action', async ()=>{
        getMovieListApi.mockResolvedValue({data:[], totalAmount:0});
        await store.dispatch(changeMovieGenreFilter(GENRES.Documentary));
        const actualResult = store.getActions();
        const expectedResult = [
            {type: ACTIONS.CHANGE_GENRE_FILTER, payload: GENRES.Documentary},
            {type: ACTIONS.GET_MOVIE_LIST, payload: {movies:[], totalAmount:0}}
        ]
        expect(actualResult).toEqual(expectedResult);
    });

    it('changeMovieSearchQuery action', async ()=>{
        getMovieListApi.mockResolvedValue({data:[], totalAmount:0});
        await store.dispatch(changeMovieSearchQuery('search string'));
        const actualResult = store.getActions();
        const expectedResult = [
            {type: ACTIONS.CHANGE_SEARCH_QUERY, payload: 'search string'},
            {type: ACTIONS.GET_MOVIE_LIST, payload: {movies:[], totalAmount:0}}
        ]
        expect(actualResult).toEqual(expectedResult);
    });

});