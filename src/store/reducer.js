import {handleActions} from 'redux-actions';
import {SORT} from '../const';
import {ACTIONS} from './types';

export const initialState = {
    sort: SORT.ReleaseDate,
    filter: [],
    movies: [],
    totalAmount: 0,
    search: ''
}

export const rootReducer = handleActions(
    {
        [ACTIONS.CHANGE_SORTING] : (state, action) => ({
            ...state,
            sort: action.payload
        }),

        [ACTIONS.CHANGE_GENRE_FILTER] : (state, action) => ({
            ...state,
            filter: [...action.payload]
        }),

        [ACTIONS.CHANGE_SEARCH_QUERY] : (state, action) => ({
            ...state,
            search: action.payload
        }),

        [ACTIONS.GET_MOVIE_LIST] : (state, action) => ({
            ...state,
            movies: [...action.payload.movies],
            totalAmount: action.payload.totalAmount
        })
    },
    initialState
  );