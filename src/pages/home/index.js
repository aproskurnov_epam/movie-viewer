import {Main} from '../../components/main';
import {getMovies} from '../../store/actions'

const Home = {
    component: Main,
    initialAction: getMovies
}

export default Home;