import {Main} from '../../components/main';
import {getMovies} from '../../store/actions'

const Film = {
    component: Main,
    initialAction: ({idFilm})=>getMovies()
}

export default Film;