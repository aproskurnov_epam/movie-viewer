import {Main} from '../../components/main';
import {changeMovieSearchQuery} from '../../store/actions'

const Search = {
    component: Main,
    initialAction: ({searchQuery})=>{
        const action = changeMovieSearchQuery(searchQuery);
        return action;
    }
}

export default Search;