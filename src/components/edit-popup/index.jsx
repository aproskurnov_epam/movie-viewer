import React, { useCallback, useMemo } from 'react';
import {PropTypes} from 'prop-types';
import {Popup} from '../popup';
import {Form} from '../form';
import {Button} from '../button';
import {Input} from '../input';
import {DateInput} from '../date-input';
import {Dropdown} from '../dropdown';
import {Field} from '../field';
import {BUTTON_APPEARANCE, BUTTON_TYPES, GENRE_OPTIONS} from '../../const';
import {useFormik} from 'formik';
import {setFormikField} from '../../helpers'
import {movieFormValidator} from '../../validators';
import {prepareGenresToForm, prepareGenresToDropdown} from '../../helpers';

export const EditPopup = ({onCloseClick, onSubmit, data}) => {

    const fieldName1 = 'title';
    const fieldName2 = 'release_date';
    const fieldName3 = 'poster_path';
    const fieldName4 = 'genres';
    const fieldName5 = 'overview';
    const fieldName6 = 'runtime';

    const initialValues = useMemo(()=>{
        const values = {};
        values[fieldName1] = data[fieldName1];
        values[fieldName2] = data[fieldName2];
        values[fieldName3] = data[fieldName3];
        values[fieldName4] = data[fieldName4];
        values[fieldName5] = data[fieldName5];
        values[fieldName6] = data[fieldName6];
        return values;
    }, [data])

    const formik = useFormik({
        initialValues,
        validate:movieFormValidator,
        onSubmit
    });

    const handleRenderControls = useCallback(({handleReset}) => {
        return (
            <>
                <Button title="Reset" onClick={handleReset} appearance={BUTTON_APPEARANCE.Transparent}/>
                <div style={{marginLeft: '10px'}}>
                    <Button title="Submit" appearance={BUTTON_APPEARANCE.Default} type={BUTTON_TYPES.Submit}/>
                </div>
            </>
        );
    },[]);

    const {
        values,
        errors,
        touched,
        handleSubmit,
        setFieldValue,
        handleReset
    } = formik

    return (
        <Popup title="Edit movie" onCloseClick={onCloseClick} >
            <Form initData={values} onSubmit={handleSubmit} onRenderControls={()=>handleRenderControls({handleReset})}>
                {({form})=>{
                    return (
                        <>
                            <Field title={fieldName1} error={touched[fieldName1] && errors[fieldName1]}>
                                <Input name={fieldName1} value={form[fieldName1]} onChange={(value)=>setFormikField(setFieldValue, value)}/>
                            </Field>
                            <Field title="Release date" error={touched[fieldName2] && errors[fieldName2]}>
                                <DateInput name={fieldName2} value={form[fieldName2]} onChange={(value)=>setFormikField(setFieldValue, value)}/>
                            </Field>
                            <Field title={"Image"} error={touched[fieldName3] && errors[fieldName3]}>
                                <Input name={fieldName3} value={form[fieldName3]} onChange={(value)=>setFormikField(setFieldValue, value)}/>
                            </Field>
                            <Field title={fieldName4} error={touched[fieldName4] && errors[fieldName4]}>
                                <Dropdown isField options={GENRE_OPTIONS.slice(1)} name={fieldName4} value={prepareGenresToDropdown(form[fieldName4])} onChange={(value)=>setFormikField(setFieldValue, prepareGenresToForm(value))}/>
                            </Field>
                            <Field title={fieldName5} error={touched[fieldName5] && errors[fieldName5]}>
                                <Input name={fieldName5} value={form[fieldName5]} onChange={(value)=>setFormikField(setFieldValue, value)}/>
                            </Field>
                            <Field title={fieldName6} error={touched[fieldName6] && errors[fieldName6]}>
                                <Input name={fieldName6} value={form[fieldName6]} onChange={({name, value})=>setFormikField(setFieldValue, {name, value:Number(value)})}/>
                            </Field>
                        </>
                    );
                }}
            </Form>
        </Popup>
    );
    
}

EditPopup.propTypes = {
    onCloseClick: PropTypes.func,
    onSubmit: PropTypes.func,
    data: PropTypes.object
}