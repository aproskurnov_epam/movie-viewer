import React, {useState} from 'react';
import {mount} from 'enzyme';
import {Form} from '../index';

jest.mock('react', ()=>({
    ...jest.requireActual('react'),
    useState: jest.fn()
}));
describe('form', ()=>{
    it('render successful', ()=>{
        const children = ({handleChangeValue})=><button data-test="change" onClick={()=>handleChangeValue({name:'title', value:'1'})}/>;
        const renderControls = ({handleReset})=><button data-test="reset" onClick={handleReset}/>
        const mockSubmit = jest.fn();
        const mockSetForm = jest.fn();
        useState.mockReturnValue([{}, mockSetForm]);
        const wrapper = mount(<Form onSubmit={mockSubmit} onRenderControls={renderControls}>{children}</Form>);
        expect(wrapper).toMatchSnapshot();

        const changeEl = wrapper.find('[data-test="change"]');
        changeEl.simulate('click');
        expect(mockSetForm).toBeCalled();

        const resetEl = wrapper.find('[data-test="reset"]');
        resetEl.simulate('click');
        expect(mockSetForm).toBeCalled();

        const formEl = wrapper.find('[data-test="form"]');
        formEl.simulate('submit');
        expect(mockSubmit).toBeCalled();
    });
});