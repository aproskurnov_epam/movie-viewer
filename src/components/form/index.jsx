import React, { useCallback, useEffect, useState } from 'react';
import {PropTypes} from 'prop-types';
import styles from './index.scss';

export const Form = ({onSubmit, onRenderControls, initData, children}) => {
    const [form, setForm] = useState({...initData});

    const handleSubmit = useCallback((e) => {
        e.preventDefault();
        onSubmit(form);
    }, [onSubmit, form]);

    const handleChangeValue = useCallback(({name, value}) => {
        setForm({...form, [name]:value});
    },[form]);

    const handleReset = useCallback(() => {
        setForm({...initData});
    },[initData]);

    useEffect(()=>{setForm({...initData})}, [initData]);

    return (
        <form data-test="form" className={styles['form']} onSubmit={handleSubmit}>
            {children({handleChangeValue, form})}
            <div className={styles['form__controls']}>
                {onRenderControls({handleReset})}
            </div>
        </form>
    );
    
}

Form.propTypes = {
    onSubmit: PropTypes.func,
    onRenderControls: PropTypes.func,
    initData: PropTypes.object
}