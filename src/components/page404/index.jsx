import React from 'react';
import {Button} from '../button';
import {Link} from 'react-router-dom';
import styles from './index.scss';
import {BUTTON_APPEARANCE} from '../../const';

export const Page404 = () => {
    return (
    <div className={styles['page404']}>
        <div className={styles['page404__text']}>Page Not Found</div>
        <Button appearance={BUTTON_APPEARANCE.Transparent} title={<Link to="/" >Go Back To Home</Link>} />
    </div>);
}