import React from 'react';
import {shallow} from 'enzyme';
import {Page404} from '../index';

describe('page 404', ()=>{

    it('render successful', ()=>{
        const wrapper = shallow(<Page404 />);
        expect(wrapper).toMatchSnapshot();
    });

});