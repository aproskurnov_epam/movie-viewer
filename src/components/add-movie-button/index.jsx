import React from 'react';
import {PropTypes} from 'prop-types';
import styles from './index.scss';

export const AddMovieButton  = ({onClick}) => {
    return (
        <>
            <button className={styles['add-movie-button']} onClick={onClick}>+ Add movie</button>
        </>
    );
}

AddMovieButton.propTypes = {
    onClick: PropTypes.func
}

