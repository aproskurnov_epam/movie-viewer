import React, {useEffect} from 'react';
import styles from './index.scss';
import {MovieItem} from '../movie-item';
import {useSelector} from 'react-redux';
import {moviesSelector} from '../../store/selectors';

export const MovieList = () => {
    const movies = useSelector(moviesSelector);
    return (
        <div className={styles['movie-list']}>
            {!Boolean(movies.length) && <div className={styles['movie-list__empty']}>No Movie Found</div>}
            {movies.map(movie=>(
                <MovieItem 
                    key={movie.id} 
                    data={movie} 
                />)
            )}
        </div>   
    );
}
