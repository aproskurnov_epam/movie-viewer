import React, { useState } from 'react';
import {shallow} from 'enzyme';
import {MovieList} from '../index';
import {GENRES} from '../../../const';
import {moviesSelector} from '../../../store/selectors';


jest.mock('react-redux', ()=>({
    useSelector: (fn)=>fn()
}));

jest.mock('../../movie-item',()=>({
    MovieItem: ()=>null
}));

jest.mock('../../../store/selectors', ()=>({
    moviesSelector: jest.fn()
}));


describe('movie list', ()=>{

    it('render successful without movies', ()=>{
        moviesSelector.mockReturnValue([]);
        const wrapper = shallow(<MovieList data={{genres:[GENRES.Documentary]}}/>);
        expect(wrapper).toMatchSnapshot();
    });

    it('render successful with movies', ()=>{
        moviesSelector.mockReturnValue([{id: 1}]);
        const wrapper = shallow(<MovieList data={{genres:[GENRES.Documentary]}}/>);
        expect(wrapper).toMatchSnapshot();
    });

});