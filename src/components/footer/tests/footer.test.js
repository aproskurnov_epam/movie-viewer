import React from 'react';
import {shallow} from 'enzyme';
import {Footer} from '../index';

describe('footer', ()=>{
    it('render successful', ()=>{
        const wrapper = shallow(<Footer />);
        expect(wrapper).toMatchSnapshot();
    });
});