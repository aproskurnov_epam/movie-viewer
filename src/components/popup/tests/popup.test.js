import React from 'react';
import {mount} from 'enzyme';
import {Popup} from '../index';
import {createMockPopupRootElement} from '../../../mock';

describe('popup', ()=>{

    it('render successful', ()=>{
        createMockPopupRootElement();
        
        const wrapper = mount(<Popup />);
        expect(wrapper).toMatchSnapshot();
    });
    
});







