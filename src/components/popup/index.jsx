import React, {useMemo, useEffect, useCallback} from 'react';
import ReactDOM from 'react-dom';
import {PropTypes} from 'prop-types';
import {useDisableMainPageScroll} from '../../hooks';
import styles from './index.scss';

export const Popup = ({title, onCloseClick, children}) => {
  useDisableMainPageScroll();

  const modalRoot = useMemo(()=>document.getElementById('popup'),[]);

  const renderPopup = useCallback(() => {
    return (
      <>
        <div className={styles['popup']}>
          <div className={styles['popup__close']} onClick={onCloseClick}/>
          <div className={styles['popup__title']}>{title}</div>
          {children}
        </div>
        <div className={styles['popup__back-layout']}/>
      </>
    );
  },[onCloseClick, title, children]);

  return ReactDOM.createPortal(
    renderPopup(),
    modalRoot
  );
}

Popup.propTypes = {
  title:PropTypes.string,
  onCloseClick: PropTypes.func
}