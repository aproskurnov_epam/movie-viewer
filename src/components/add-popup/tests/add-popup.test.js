import React from 'react';
import {mount} from 'enzyme';
import {AddPopup} from '../index';
import {createMockPopupRootElement} from '../../../mock';
import {setFormikField} from '../../../helpers'
import { GENRES } from '../../../const';

const formikObj = {
    errors:{}, 
    touched:{
        'title':true,
        'release_date':true,
        'poster_path':true,
        'genres':true,
        'overview':true,
        'runtime':true
    }
}

jest.mock('formik', ()=>({
    useFormik: ()=>formikObj
}));

jest.mock('../../../helpers', ()=>({
    setFormikField:jest.fn(),
    prepareGenresToDropdown: jest.fn(),
    prepareGenresToForm: jest.fn()
}));

describe('add popup', ()=>{

    it('render successful', ()=>{
        createMockPopupRootElement();

        const wrapper = mount(<AddPopup />);
        expect(wrapper).toMatchSnapshot();

        let el = wrapper.find('input[name="title"]');
        el.simulate('change', '1');
        expect(setFormikField).toBeCalled();

        el = wrapper.find('input[name="release_date"]');
        el.simulate('change', '1');
        expect(setFormikField).toBeCalled();

        el = wrapper.find('input[name="poster_path"]');
        el.simulate('change', '1');
        expect(setFormikField).toBeCalled();

        el = wrapper.find('input[name="genres"]');
        el.simulate('change', GENRES.Documentary);
        expect(setFormikField).toBeCalled();

        el = wrapper.find('input[name="overview"]');
        el.simulate('change', '1');
        expect(setFormikField).toBeCalled();

        el = wrapper.find('input[name="runtime"]');
        el.simulate('change', '1');
        expect(setFormikField).toBeCalled();
    });
    
});