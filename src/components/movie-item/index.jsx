import React, {useCallback, useState} from 'react';
import {PropTypes} from 'prop-types';
import styles from './index.scss';
import {Menu} from '../menu';
import {EditPopup} from '../edit-popup';
import {DeletePopup} from '../delete-popup';
import {deleteMovie, updateMovie} from '../../store/actions';
import {useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';


export const MovieItem = ({data}) => {
    const [isMenuActive, setMenuActive] = useState(false);
    const [isEditPopupVisible, setEditPopupVisiblity] = useState(false);
    const [isDeletePopupVisible, setDeletePopupVisiblity] = useState(false);

    const history = useHistory();
    const dispatch = useDispatch();

    const {id, title, poster_path, release_date, genres} = data;

    const showEditMoviePopup = useCallback(() => {
        setEditPopupVisiblity(true);
    },[]);

    const hideEditMoviePopup = useCallback(() => {
        setEditPopupVisiblity(false);
    },[]);

    const showDeleteMoviePopup = useCallback(() => {
        setDeletePopupVisiblity(true);
    },[]);

    const hideDeleteMoviePopup = useCallback(() => {
        setDeletePopupVisiblity(false);
    },[]);

    const handleEditMovie = useCallback((data) => {
        hideEditMoviePopup();
        dispatch(updateMovie(data));
    },[hideEditMoviePopup]);

    const handleDeleteMovie = useCallback((id) => {
        hideDeleteMoviePopup();
        dispatch(deleteMovie(id));
    },[hideDeleteMoviePopup]);

    const handleSelectMovie = useCallback(()=>{
        history.push(`/film/${id}`)
    }, [id]);

    return (
        <div data-test="item" className={styles['movie-item']} onMouseEnter={()=>setMenuActive(true)} onMouseLeave={()=>setMenuActive(false)}>
            <img data-test="select-movie" className={styles['movie-item__poster']} src={`${poster_path}`} onClick={handleSelectMovie}/>
            <div className={styles['movie-item__info']}>
                <div className={styles['movie-item__info-top']}>
                    <div className={styles['movie-item__title']}>{title}</div>
                    <div className={styles['movie-item__genre']}>{genres.reduce((prev, current)=>prev+' ' + current, '')}</div>
                </div>
                <div className={styles['movie-item__release-date']}>{release_date}</div>
            </div>
            {isMenuActive && <Menu onEdit={showEditMoviePopup} onDelete={showDeleteMoviePopup}/>}
            {isEditPopupVisible && <EditPopup data={data} onCloseClick={hideEditMoviePopup} onSubmit={handleEditMovie}/>}
            {isDeletePopupVisible && <DeletePopup id={id} onCloseClick={hideDeleteMoviePopup} onSubmit={handleDeleteMovie}/>}
        </div>
    );
}

MovieItem.propTypes = {
    data: PropTypes.object,
}