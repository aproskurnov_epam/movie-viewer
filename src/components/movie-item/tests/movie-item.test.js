import React, { useState } from 'react';
import {mount, shallow} from 'enzyme';
import {MovieItem} from '../index';
import {GENRES} from '../../../const';
import {useDispatch} from 'react-redux';

jest.mock('react', ()=>({
    ...jest.requireActual('react'),
    useState: jest.fn()
}))

jest.mock('react-redux', ()=>({
    useDispatch: jest.fn()
}));

const mockPush = jest.fn();
jest.mock('react-router-dom', ()=>({
    useHistory: ()=>({push:mockPush})
}));

jest.mock('../../menu',()=>({
    Menu: ({onEdit, onDelete})=>(
        <>
            <button data-test="edit" onClick={onEdit} />
            <button data-test="delete" onClick={onDelete} />
        </>
    )
}));

jest.mock('../../edit-popup',()=>({
    EditPopup: ({onCloseClick, onSubmit})=>(
        <>
            <button data-test="edit-close" onClick={onCloseClick} />
            <button data-test="edit-submit" onClick={onSubmit} />
        </>
    )
}));

jest.mock('../../delete-popup',()=>({
    DeletePopup: ({onCloseClick, onSubmit})=>(
        <>
            <button data-test="delete-close" onClick={onCloseClick} />
            <button data-test="delete-submit" onClick={onSubmit} />
        </>
    )
}));


const mockSetState = jest.fn();

describe('movie item', ()=>{

    beforeEach(()=>{
        useState.mockReturnValue([false, mockSetState]);
    });

    it('render successful', ()=>{
        const wrapper = shallow(<MovieItem data={{genres:[GENRES.Documentary]}}/>);
        expect(wrapper).toMatchSnapshot();
    });

    it('mouse enter and leave, select movie click', ()=>{
        const wrapper = mount(<MovieItem data={{genres:[GENRES.Documentary]}}/>);

        wrapper.find('[data-test="item"]').simulate('mouseEnter');
        expect(mockSetState).toBeCalledWith(true);

        wrapper.find('[data-test="item"]').simulate('mouseLeave');
        expect(mockSetState).toBeCalledWith(false);

        wrapper.find('[data-test="select-movie"]').simulate('click');
        expect(mockPush).toBeCalled();
        
    });

    it('rest action', ()=>{
        useState.mockReturnValue([true, mockSetState]);
        const mockDispatch = jest.fn();
        useDispatch.mockReturnValue(mockDispatch);
        const wrapper = mount(<MovieItem data={{genres:[GENRES.Documentary]}}/>);

        wrapper.find('[data-test="edit"]').simulate('click');
        expect(mockSetState).toBeCalled();

        wrapper.find('[data-test="delete"]').simulate('click');
        expect(mockSetState).toBeCalled();

        wrapper.find('[data-test="edit-close"]').simulate('click');
        expect(mockSetState).toBeCalled();

        wrapper.find('[data-test="delete-close"]').simulate('click');
        expect(mockSetState).toBeCalled();

        wrapper.find('[data-test="edit-submit"]').simulate('click');
        expect(mockDispatch).toBeCalled();

        wrapper.find('[data-test="delete-submit"]').simulate('click');
        expect(mockDispatch).toBeCalled();
    });

});