import React from 'react';
import styles from './index.scss';
import {totalAmountSelector} from '../../store/selectors';
import {useSelector} from 'react-redux';

export const Counter  = () => {
    const count = useSelector(totalAmountSelector);
    if (!count){
        return null;
    }
    return (
        <div className={styles['counter']}><strong>{count}</strong> movies found</div>
    );
}