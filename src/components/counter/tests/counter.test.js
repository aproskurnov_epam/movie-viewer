import React from 'react';
import {shallow} from 'enzyme';
import {Counter} from '../index';
import {totalAmountSelector} from '../../../store/selectors';

jest.mock('react-redux', ()=>({
    useSelector: (fn)=>fn()
}))
jest.mock('../../../store/selectors', ()=>({
    totalAmountSelector: jest.fn()
}))

describe('counter', ()=>{
    it('render successful with count', ()=>{
        totalAmountSelector.mockReturnValue(10);
        const wrapper = shallow(<Counter />);
        expect(wrapper).toMatchSnapshot();
    });

    it('render empty', ()=>{
        totalAmountSelector.mockReturnValue(0);
        const wrapper = shallow(<Counter />);
        expect(wrapper).toMatchSnapshot();
    });
});