import React from 'react';
import styles from './index.scss';
import {FilterPanel} from '../filter-panel';
import {SortPanel} from '../sort-panel';
import { GENRES } from '../../const';

export const ControlPanel = () => {
    return (
        <div className={styles['control-panel']}>
            <FilterPanel/>
            <SortPanel />
        </div>   
    );
}
