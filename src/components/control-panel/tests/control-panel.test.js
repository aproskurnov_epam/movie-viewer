import React from 'react';
import {shallow} from 'enzyme';
import {ControlPanel} from '../index';

describe('control-panel', ()=>{
    it('render successful', ()=>{
        const wrapper = shallow(<ControlPanel />);
        expect(wrapper).toMatchSnapshot();
    });
});