import React, { useCallback, useState } from 'react';
import {PropTypes} from 'prop-types';
import styles from './index.scss';
import {Dropdown} from '../dropdown';
import {useDispatch, useSelector} from 'react-redux';
import {SORT_OPTIONS} from '../../const';
import {sortSelector} from '../../store/selectors';
import {changeMovieSorting} from '../../store/actions';

export const SortPanel = () => {
    const sort = useSelector(sortSelector);
    const [value, setValue] = useState(sort);
    const dispatch = useDispatch();
    
    const handleChangeValue = useCallback(({value:newValue})=>{
        setValue(newValue);
        dispatch(changeMovieSorting(newValue));
    }, []);
    return (
        <div className={styles['sort-panel']}>
            <div className={styles['sort-panel__pretext']}>SORT BY</div>
            <div className={styles['sort-panel__control']}>
                <Dropdown name="sort" options={SORT_OPTIONS} value={value} onChange={handleChangeValue}/>
            </div>
        </div> 
    );  
}