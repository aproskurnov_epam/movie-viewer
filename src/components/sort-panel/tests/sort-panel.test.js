import React, { useState } from 'react';
import {mount, shallow} from 'enzyme';
import {SortPanel} from '../index';

jest.mock('../../../store/selectors', ()=>({sortSelector: ()=>''}));

jest.mock('react', ()=>({
    ...jest.requireActual('react'),
    useState: jest.fn()
}))

const mockDispatch = jest.fn();
jest.mock('react-redux', ()=>({
    useDispatch: ()=>mockDispatch,
    useSelector: (fn)=>fn()
}));

jest.mock('../../dropdown',()=>({
    Dropdown: ({onChange})=>(
            <button data-test="change" onClick={onChange} />
    )
}));

const mockSetState = jest.fn();

describe('search', ()=>{

    beforeEach(()=>{
        useState.mockReturnValue(['', mockSetState]);
    });

    it('render successful', ()=>{
        const wrapper = shallow(<SortPanel/>);
        expect(wrapper).toMatchSnapshot();
    });

    it('change sort', ()=>{
        const wrapper = mount(<SortPanel/>);

        wrapper.find('[data-test="change"]').simulate('click');
        expect(mockDispatch).toBeCalled();
        
    });

});