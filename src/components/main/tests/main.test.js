import React from 'react';
import {mount, shallow} from 'enzyme';
import {Main} from '../index';
import {useParams} from 'react-router-dom';
import {useDispatch} from 'react-redux';

jest.mock('react-router-dom', ()=>({
    useParams: jest.fn(),
}));

jest.mock('react-redux', ()=>({
    useDispatch: jest.fn()
}));

const Empty = ()=>null;

jest.mock('../../header', ()=>({
    Header: Empty
}));

jest.mock('../../content', ()=>({
    Content: Empty
}));

jest.mock('../../footer', ()=>({
    Footer: Empty
}));

describe('main', ()=>{

    beforeEach(()=>{
        useParams.mockReturnValue({idFilm: null, searchQuery: null});
    });

    it('render successful', ()=>{
        const wrapper = shallow(<Main/>);
        expect(wrapper).toMatchSnapshot();
    });

    it('search query in', ()=>{
        const mockDispatch = jest.fn();
        useDispatch.mockReturnValue(mockDispatch);
        useParams.mockReturnValue({idFilm: null, searchQuery: 'search query'});
        mount(<Main/>);
        expect(mockDispatch).toBeCalled();
    });

    it('id film in', ()=>{
        const mockDispatch = jest.fn();
        useDispatch.mockReturnValue(mockDispatch);
        useParams.mockReturnValue({idFilm: 1, searchQuery: null});
        mount(<Main/>);
        expect(mockDispatch).toBeCalled();
    });
});