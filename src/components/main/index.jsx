import React, { useEffect } from 'react';
import {Header} from '../header';
import {Content} from '../content';
import {Footer} from '../footer';
import {useDispatch} from 'react-redux';
import {changeMovieSearchQuery, getMovies} from '../../store/actions';
import {useParams} from 'react-router-dom';

export const Main = () => {
    const dispatch = useDispatch();
    const {searchQuery, idFilm} = useParams();

    useEffect(()=>{
        if(searchQuery){
            dispatch(changeMovieSearchQuery(searchQuery));
        }
        if (idFilm){
            dispatch(getMovies());
        }
        
    },[searchQuery, idFilm]);

    return (
            <>
                <Header />
                <Content />
                <Footer />
            </>
        );
}

