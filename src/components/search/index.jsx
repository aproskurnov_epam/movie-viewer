import React, {useState} from 'react';
import styles from './index.scss';
import {Button} from '../button';
import {useHistory, useParams} from 'react-router-dom';

export const Search  = () => {
    const {searchQuery} = useParams();
    const history = useHistory();
    const [searchStr, setSearchStr] = useState(searchQuery || '');
    const search = () => {
        history.push(`/search/${encodeURI(searchStr)}`);
    }
    return (
        <div className={styles['search']}>
            <input data-test="change" value={searchStr} onChange={({target:{value}})=>setSearchStr(value)} className={styles['search__input']} type="text" placeholder="What do you want to watch"/>
            <Button onClick={search} title="Search"/>
        </div>
    )
}
