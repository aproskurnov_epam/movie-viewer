import React, { useState } from 'react';
import {mount, shallow} from 'enzyme';
import {Search} from '../index';

jest.mock('react', ()=>({
    ...jest.requireActual('react'),
    useState: jest.fn()
}))

const mockPush = jest.fn();
jest.mock('react-router-dom', ()=>({
    useParams: ()=>jest.fn(),
    useHistory: ()=>({push:mockPush})
}));

jest.mock('../../button',()=>({
    Button: ({onClick})=>(
            <button data-test="search" onClick={onClick} />
    )
}));

const mockSetState = jest.fn();

describe('search', ()=>{

    beforeEach(()=>{
        useState.mockReturnValue(['', mockSetState]);
    });

    it('render successful', ()=>{
        const wrapper = shallow(<Search/>);
        expect(wrapper).toMatchSnapshot();
    });

    it('mount, click and change', ()=>{
        const wrapper = mount(<Search/>);

        wrapper.find('[data-test="change"]').simulate('change');
        expect(mockSetState).toBeCalled();

        wrapper.find('[data-test="search"]').simulate('click');
        expect(mockPush).toBeCalled();
        
    });

});