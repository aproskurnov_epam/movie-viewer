import React, { useState } from 'react';
import {mount, shallow} from 'enzyme';
import {Menu} from '../index';

jest.mock('react', ()=>({
    ...jest.requireActual('react'),
    useState: jest.fn()
}))

const mockSetState = jest.fn();

describe('menu', ()=>{

    beforeEach(()=>{
        useState.mockReturnValue([false, mockSetState]);
    });

    it('render successful', ()=>{
        const wrapper = shallow(<Menu/>);
        expect(wrapper).toMatchSnapshot();
    });

    it('click open button, menu is closed', ()=>{
        const wrapper = mount(<Menu/>);

        wrapper.find('[data-test="open"]').simulate('click');
        expect(mockSetState).toBeCalledWith(true);
        
    });

    it('click all button, menu is open', ()=>{
        useState.mockReturnValue([true, mockSetState]);
        const mockEdit = jest.fn();
        const mockDelete = jest.fn();
        const wrapper = mount(<Menu onEdit={mockEdit} onDelete={mockDelete}/>);
        
        wrapper.find('[data-test="close"]').simulate('click');
        expect(mockSetState).toBeCalledWith(false);

        wrapper.find('[data-test="edit"]').simulate('click');
        expect(mockEdit).toBeCalled();

        wrapper.find('[data-test="delete"]').simulate('click');
        expect(mockDelete).toBeCalled();
        
    });

});