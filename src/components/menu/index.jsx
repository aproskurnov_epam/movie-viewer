import React, {useCallback, useState} from 'react';
import {PropTypes} from 'prop-types';
import styles from './index.scss';

export const Menu = ({onEdit, onDelete}) => {
    const [isOpen, setOpen] = useState(false);

    const handleEdit = useCallback(() => {
        setOpen(false);
        onEdit();
    },[onEdit]);

    const handleDelete = useCallback(() => {
        setOpen(false);
        onDelete();
    },[onDelete])

    return <div className={styles['menu']}>
        {!isOpen && <div data-test="open" className={styles['menu__button']} onClick={()=>setOpen(true)}/>}
        {isOpen && <>
            <div data-test="close" className={styles['menu__close']} onClick={()=>setOpen(false)}/>
            <div className={styles['menu__container']}>
                <div data-test="edit" className={styles['menu__item']} onClick={handleEdit}>Edit</div>
                <div data-test="delete" className={styles['menu__item']} onClick={handleDelete}>Delete</div>
            </div>
        </>}
    </div> 
}

Menu.propTypes = {
    onEdit: PropTypes.func,
    onDelete: PropTypes.func,
}