import React, {useMemo, useState, useCallback} from 'react';
import {AddMovieButton} from '../add-movie-button';
import {AddPopup} from '../add-popup';
import {Search} from '../search';
import {Logo} from '../logo';
import {Details} from '../details';
import styles from './index.scss';
import SearchIco from './search.png';
import {moviesSelector} from '../../store/selectors';
import {createMovie} from '../../store/actions';
import {useParams, useHistory} from 'react-router-dom';

import {useDispatch, useSelector} from 'react-redux';

export const Header  = () => {
    const [isAddPopupVisible, setAddPopupVisiblity] = useState(false);
    const {idFilm} = useParams();
    const history = useHistory();

    const movies = useSelector(moviesSelector);
    const dispatch = useDispatch();

    const movie = useMemo(()=>idFilm && movies.find(movie=>movie.id === Number(idFilm)), [idFilm, movies]);
    const isDetailsShow = Boolean(movie);

    const showAddMoviePopup = useCallback(() => {
        setAddPopupVisiblity(true);
    },[]);

    const hideAddMoviePopup = useCallback(() => {
        setAddPopupVisiblity(false);
    },[]);

    const handleSubmitAddMovie = useCallback((form) => {
        dispatch(createMovie(form));
        setAddPopupVisiblity(false);
    },[]);

    return (
        <div className={styles['header']}>
            <div className={styles['header__top']}>
                <Logo />
                {isDetailsShow 
                    ? <img data-test="searchIco" src={SearchIco} onClick={()=>{history.push('/')}} />
                    : <AddMovieButton onClick={showAddMoviePopup}/> }
            </div>
            <div className={styles['header__data-panel']}>
            {isDetailsShow 
                ?   <Details movie={movie} />
                :   (
                    <>
                        <div className={styles['header__search-title']}>Find your movie</div>
                        <Search />
                    </>
                )
            }
            </div>
            {isAddPopupVisible && <AddPopup onCloseClick={hideAddMoviePopup} onSubmit={handleSubmitAddMovie}/>}
        </div>
    );
}