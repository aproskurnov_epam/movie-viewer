import React, { useState } from 'react';
import {mount} from 'enzyme';
import {Header} from '../index';
import {useParams} from 'react-router-dom';
import {useDispatch} from 'react-redux';

const mockPush = jest.fn();

jest.mock('react', ()=>({
    ...jest.requireActual('react'),
    useState: jest.fn()
}));

jest.mock('react-router-dom', ()=>({
    useParams: jest.fn(),
    useHistory: ()=>({push: mockPush})
}));

jest.mock('react-redux', ()=>({
    useSelector: (fn)=>fn(),
    useDispatch: jest.fn()
}));

jest.mock('../../../store/selectors', ()=>({
    moviesSelector: ()=>[{id:1}]
}));

jest.mock('../../add-popup', ()=>({
    AddPopup: ({onCloseClick,onSubmit})=>{
        return (
            <>
                <div data-test="close" onClick={onCloseClick} />
                <div data-test="submit" onClick={onSubmit} />
            </>
        );
    }})
);

jest.mock('../../details', ()=>({
    Details: ()=>null
}));


jest.mock('../../add-movie-button', ()=>({
    AddMovieButton: ({onClick})=><button data-test="click" onClick={onClick} />
}))
const mockSetState = jest.fn();

describe('header', ()=>{

    beforeEach(()=>{
        useState.mockReturnValue([false, mockSetState]);
    });

    it('render successful search panel', ()=>{
        useParams.mockReturnValue({idFilm:null});
        const wrapper = mount(<Header/>);
        expect(wrapper).toMatchSnapshot();
    });

    it('open add movie popup, change visiblity', ()=>{
        useParams.mockReturnValue({idFilm:null});
        const wrapper = mount(<Header/>);

        const addMovieEl = wrapper.find('[data-test="click"]');
        addMovieEl.simulate('click');
        expect(mockSetState).toBeCalled();
    });

    it('open add movie popup, submit/close', ()=>{
        useState.mockReturnValue([true, mockSetState]);
        const mockDispatch = jest.fn();
        useDispatch.mockReturnValue(mockDispatch);

        const wrapper = mount(<Header/>);
        
        const closeEl = wrapper.find('[data-test="close"]');
        closeEl.simulate('click');
        expect(mockSetState).toBeCalled();

        const submitEl = wrapper.find('[data-test="submit"]');
        submitEl.simulate('click');
        expect(mockDispatch).toBeCalled();

    });

    it('render successful details panel, click go search', ()=>{
        useParams.mockReturnValue({idFilm:1});
        const wrapper = mount(<Header/>);
        expect(wrapper).toMatchSnapshot();

        const goSearchEl = wrapper.find('[data-test="searchIco"]');
        goSearchEl.simulate('click');
        expect(mockPush).toBeCalled();
    });

});