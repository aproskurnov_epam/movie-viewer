import React from 'react';
import {shallow} from 'enzyme';
import {Logo} from '../index';

describe('logo', ()=>{
    it('render successful', ()=>{
        const wrapper = shallow(<Logo />);
        expect(wrapper).toMatchSnapshot();
    });
});