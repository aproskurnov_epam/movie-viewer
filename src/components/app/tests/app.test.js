import React from 'react';
import {mount} from 'enzyme';
import {App} from '../index';

jest.mock('../../../helpers', ()=>({
    setFormikField:jest.fn(),
    prepareGenresToDropdown: jest.fn(),
    prepareGenresToForm: jest.fn()
}));




describe('app', ()=>{

    it('render successful', ()=>{
        const wrapper = mount(<App />);
        expect(wrapper).toMatchSnapshot();
    });
    
});