import React from 'react';
import {ErrorBoundary} from '../error-boundary';
import {Switch, Route} from 'react-router-dom';
import routes from '../../routes';
import { Provider } from 'react-redux';

export const App  = ({Router, store, location}) => {
    return (
        <Router location={location}>
            <Provider store={store}>
                <ErrorBoundary>
                        <Switch>
                            {routes.map(route=>{
                                const {exact, path, component} = route;
                                return <Route key={path} exact={exact} path={path} component={component}/>    
                            })}
                        </Switch>
                </ErrorBoundary>
            </Provider>
        </Router>
    );
}