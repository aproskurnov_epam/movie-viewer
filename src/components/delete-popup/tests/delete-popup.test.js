import React from 'react';
import {mount} from 'enzyme';
import {DeletePopup} from '../index';
import {createMockPopupRootElement} from '../../../mock';

describe('delete popup', ()=>{
    it('render successful', ()=>{
        createMockPopupRootElement();
        const submitMock = jest.fn();
        const wrapper = mount(<DeletePopup onSubmit={submitMock}/>);
        expect(wrapper).toMatchSnapshot();

        const el = wrapper.find('[title="Confirm"]');
        el.simulate('click');
        expect(submitMock).toBeCalled();
    }); 
});