import React, {useCallback} from 'react';
import {PropTypes} from 'prop-types';
import {Popup} from '../popup';
import {Form} from '../form';
import {Button} from '../button';
import {BUTTON_APPEARANCE} from '../../const';
import styles from './index.scss';

export const DeletePopup = ({onCloseClick, onSubmit, id}) => {

    const handleRenderControls = useCallback(() => {
        return (
                <Button title="Confirm" onClick={()=>onSubmit(id)} appearance={BUTTON_APPEARANCE.Default}/>
        );
    },[onSubmit]);

    return (
        <Popup title="Delete movie" onCloseClick={onCloseClick} >
            <Form onSubmit={onSubmit} onRenderControls={handleRenderControls}>
                {()=>{
                    return (
                        <div className={styles['delete-popup']}>
                            Are you sure you want to delete this form?
                        </div>
                    );
                }}
            </Form>
        </Popup>
    );
    
}

DeletePopup.propTypes = {
    onCloseClick: PropTypes.func,
    onSubmit: PropTypes.func,
    id: PropTypes.number
}