import React from 'react';
import {PropTypes} from 'prop-types';
import styles from './index.scss';
import {BUTTON_TYPES, BUTTON_APPEARANCE} from '../../const';

export const Button  = ({appearance=BUTTON_APPEARANCE.Default, title, type=BUTTON_TYPES.Button, onClick}) => (
    <button type={type} className={`${styles['button']} ${styles[`button--${appearance}`]}`} onClick={onClick}>{title}</button>
)

Button.propTypes = {
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    type: PropTypes.string,
    appearance: PropTypes.string,
    onClick: PropTypes.func
}