import React from 'react';
import {shallow} from 'enzyme';
import {Button} from '../index';

describe('button', ()=>{
    it('render successful', ()=>{
        const wrapper = shallow(<Button />);
        expect(wrapper).toMatchSnapshot();
    });
});