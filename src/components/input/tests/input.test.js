import React from 'react';
import {mount, shallow} from 'enzyme';
import {Input} from '../index';

describe('input', ()=>{
    it('render successful', ()=>{
        const wrapper = shallow(<Input />);
        expect(wrapper).toMatchSnapshot();
    });

    it('change value', ()=>{
        const mockChange = jest.fn();
        const wrapper = mount(<Input onChange={mockChange}/>);
        wrapper.find('input').simulate('change', '1');
        expect(mockChange).toBeCalled();
    });
});