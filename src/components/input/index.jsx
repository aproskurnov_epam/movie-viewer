import React, { useCallback } from 'react';
import {PropTypes} from 'prop-types';
import styles from './index.scss';

export const Input = ({ name, value, onChange }) => {

    const handleChange = useCallback(({target:{value:newValue}}) => {
        onChange({name, value:newValue});
    },[onChange, name]);

    return (
        <input name={name} className={styles['input']} value={value || ''} onChange={handleChange}/>
    );    
}

Input.propTypes = {
    name: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChange: PropTypes.func
}

