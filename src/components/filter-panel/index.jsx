import React, {useCallback} from 'react';
import styles from './index.scss';
import {GENRES } from '../../const';
import {useDispatch, useSelector} from 'react-redux';
import {filterSelector} from '../../store/selectors';
import {changeMovieGenreFilter} from '../../store/actions';

export const FilterPanel  = () => {
    const activeFilter = useSelector(filterSelector);
    const dispatch = useDispatch();

    const handleSelectFilter = useCallback((filter) => {
        dispatch(changeMovieGenreFilter(filter));
    });
    return (
        <div className={styles['filter-panel']}>
            {Object.values(GENRES).map(filter=>{
                const isCurFilterActive = (filter === GENRES.All && !activeFilter.length) || (activeFilter.length && activeFilter[0] === filter);
                return (
                    <div 
                        key={filter} 
                        data-test="item"
                        className={`${styles['filter-panel__item']} 
                        ${ isCurFilterActive && styles['filter-panel__item--active']}`}
                        onClick={()=>handleSelectFilter([filter])}
                    >
                        {filter}
                    </div>
                )
            })}
        </div>   
    );
}
