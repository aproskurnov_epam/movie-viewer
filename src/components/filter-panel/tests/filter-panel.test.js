import React from 'react';
import {mount, shallow} from 'enzyme';
import {FilterPanel} from '../index';
import {useDispatch} from 'react-redux';
import { GENRES } from '../../../const';
import {filterSelector} from '../../../store/selectors'

jest.mock('react-redux', ()=>({
    useDispatch: jest.fn(),
    useSelector: (fn)=>fn()
}));

jest.mock('../../../store/selectors', ()=>({
    filterSelector: jest.fn()
}))

describe('filter panel', ()=>{
    it('render successful, all filter selected', ()=>{
        filterSelector.mockReturnValue([]);
        const mockDispatch = jest.fn();
        useDispatch.mockReturnValue(mockDispatch);
        const wrapper = mount(<FilterPanel />);
        expect(wrapper).toMatchSnapshot();

        const itemEl = wrapper.find('[data-test="item"]').at(0);
        itemEl.simulate('click');
        expect(mockDispatch).toBeCalled();
    });

    it('render successful, concrete filter selected', ()=>{
        filterSelector.mockReturnValue([GENRES.Documentary]);
        const wrapper = shallow(<FilterPanel />);
        expect(wrapper).toMatchSnapshot();
    });
});