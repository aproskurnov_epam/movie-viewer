import React from 'react';
import {mount} from 'enzyme';
import {Details} from '../index';
import {GENRES} from '../../../const';

describe('details', ()=>{
    it('render successful', ()=>{
        const movie = {
            title:'mock',
            poster_path:'mock',
            release_date: 'mock',
            genres: [GENRES.Documentary],
            overview: 'mock', 
            runtime: 'movk',
            vote_average: 'mock'
        }
        const wrapper = mount(<Details movie={movie}/>);
        expect(wrapper).toMatchSnapshot();
    }); 
});