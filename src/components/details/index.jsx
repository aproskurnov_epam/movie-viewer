import React, {useMemo} from 'react';
import {PropTypes} from 'prop-types';
import styles from './index.scss';

export const Details  = ({movie}) => {
    const {title, poster_path, release_date, genres, overview, runtime, vote_average} = movie;
    return (
        <div className={styles['details']}>
            <img className={styles['details__poster']} src={poster_path}/>
            <div className={styles['details__info']} >
                <div className={styles['details__row']} >
                    <div className={styles['details__title']} >{title}</div>
                    <div className={styles['details__rating']} >{vote_average}</div>
                </div>
                <div className={styles['details__genre']} >{genres.reduce((prev, current)=>prev+' ' + current, '')}</div>
                <div className={styles['details__row']} >
                    <div className={styles['details__release']}>{release_date}</div>
                    <div className={styles['details__duration']}>{runtime}<span> min</span></div>
                </div>
                <div className={styles['details__description']} >
                    {overview}
                </div>
            </div>
        </div>
    );
}

Details.propTypes = {
    movie: PropTypes.object,
}