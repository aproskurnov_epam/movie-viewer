import React from 'react';
import {ControlPanel} from '../control-panel';
import {Counter} from '../counter';
import {MovieList} from '../movie-list';
import styles from './index.scss';

export const Content  = () => {
    return (
        <div className={styles['content']}>
            <ControlPanel />
            <Counter/>
            <MovieList/>
        </div>   
    );
}