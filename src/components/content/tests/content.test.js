import React from 'react';
import {shallow} from 'enzyme';
import {Content} from '../index';

describe('content', ()=>{
    it('render successful', ()=>{
        const wrapper = shallow(<Content />);
        expect(wrapper).toMatchSnapshot();
    });
});