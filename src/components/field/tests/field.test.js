import React from 'react';
import {shallow} from 'enzyme';
import {Field} from '../index';

describe('field', ()=>{
    it('render successful', ()=>{
        const wrapper = shallow(<Field />);
        expect(wrapper).toMatchSnapshot();
    });
});