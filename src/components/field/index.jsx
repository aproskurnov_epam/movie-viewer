import React from 'react';
import PropTypes from 'prop-types';
import styles from './index.scss';

export const Field  = ({title, error, children}) => {
    return (
        <div className={styles['field']}>
            <div className={styles['field__title']}>{title}</div>
            <div className={styles['field__error']}>{error}</div>
            <div className={styles['field__control']}>
                {children}
            </div>
        </div>   
    );
}

Field.propTypes = {
    title: PropTypes.string,
    error: PropTypes.string
}