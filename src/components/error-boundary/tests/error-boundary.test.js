import React from 'react';
import {mount} from 'enzyme';
import {ErrorBoundary} from '../index';

describe('error boundary', ()=>{
    it('render successful all ok', ()=>{
        const wrapper = mount(<ErrorBoundary>all right</ErrorBoundary>);
        expect(wrapper).toMatchSnapshot();
    });

    it('render successful something went wrong', ()=>{
        const WrongObj = () => {
            throw new Error('wrong');
        }
        const wrapper = mount(<ErrorBoundary><WrongObj/></ErrorBoundary>);
        expect(wrapper).toMatchSnapshot();
    }); 
});