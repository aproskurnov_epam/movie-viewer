import React from 'react';
import {mount} from 'enzyme';
import {DateInput} from '../index';
import {setFormikField} from '../../../helpers'


describe('date input', ()=>{
    it('render successful', ()=>{
        const mockChange = jest.fn();
        const wrapper = mount(<DateInput onChange={mockChange} name='title'/>);
        expect(wrapper).toMatchSnapshot();

        const el = wrapper.find('input[name="title"]');
        el.simulate('change', '1');
        expect(mockChange).toBeCalled();
    }); 
});