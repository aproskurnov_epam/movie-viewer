import React, {useState, useCallback} from "react";
import {PropTypes} from 'prop-types';
import styles from "./index.scss";

export const Dropdown = ({options, name, value, onChange, isField}) => {
  const [isExpanded, setExpand] = useState(false);

  const handleChange = useCallback((newValue) => {
    setExpand(false);
    onChange({name, value: newValue});
  }, [onChange, name]);

  const handleExpand = useCallback(() => {
    setExpand(true);
  },[]);

  return (
    <div className={styles['dropdown']}>
        <div data-test="base-element" className={`${styles['dropdown__selected-item']} ${isField?styles['dropdown__selected-item--field']:''}`} onClick={handleExpand}>
          {options.find(option=>option.value === value)?.label}
        </div>
        {isExpanded && Boolean(options.length) && <div className={styles['dropdown__container']}>
            {options.map(item => (
              <div 
                key={item.value}
                data-test="item"
                className={styles['dropdown__item']}
                onClick={()=>handleChange(item.value)}
              >
                {item.label}
              </div>
            ))}
        </div>}
      <input name={name} className={styles['dropdown__real-element']} value={value || ''} onChange={handleChange}/>
    </div>
  );
}

Dropdown.propTypes = {
  options: PropTypes.array,
  name: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  isField: PropTypes.bool
};
