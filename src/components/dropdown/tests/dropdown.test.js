import React from 'react';
import {mount} from 'enzyme';
import {Dropdown} from '../index';

describe('dropdown', ()=>{
    it('render successful', ()=>{
        const changeMock = jest.fn();
        const wrapper = mount(<Dropdown options={[{label: 'mockLabel', value: 'mockValue'}]} onChange={changeMock}/>);
        expect(wrapper).toMatchSnapshot();

        const baseEl = wrapper.find('[data-test="base-element"]');
        baseEl.simulate('click');
        const itemEl = wrapper.find('[data-test="item"]');
        itemEl.simulate('click');

        expect(changeMock).toBeCalled();
    }); 
});