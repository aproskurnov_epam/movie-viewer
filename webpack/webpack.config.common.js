
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: "production",
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'bundle.js',
        publicPath: "/"
    },
    resolve: {
        extensions: [".js", ".jsx"]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: './src/index.html'
        }),
        
    ],
    module:{
        rules: [
            {
                test: /\.js(x?)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader"
                    }
                ],
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        loader: 'css-loader',
                        options: {modules: true}
                    },
                    { loader: 'sass-loader'}
                ]
            },
                {
                    test: /\.png$/i,
                    use: [
                        {
                            loader: 'file-loader',
                        },
                    ],
                },
        ]
    }
};