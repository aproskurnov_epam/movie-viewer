const { merge, mergeWithRules, CustomizeRule } = require('webpack-merge');
const common = require('./webpack.config.common');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const mergedConfig = merge(common, {
  mode: 'production',
  plugins: [new MiniCssExtractPlugin()]
});

const miniCssPluginMergingRules = {
  module: {
    rules:{
      test: CustomizeRule.Match,
      use: CustomizeRule.Prepend 
    }
  }
};

const miniCssPluginRules = {
  module: {
    rules:[
      {
        test: /\.(scss|css)$/,
        use: [MiniCssExtractPlugin.loader]
      }
    ]
  }
}

module.exports = mergeWithRules(miniCssPluginMergingRules)(mergedConfig, miniCssPluginRules);

