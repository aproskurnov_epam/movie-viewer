const nodeExternals = require('webpack-node-externals');
const path = require('path');

const isDevMod = process.env.NODE_ENV === 'development';

module.exports = {
    mode: process.env.NODE_ENV,
  name: 'server',
  target: 'node',
  entry: './src/serverRenderer.js',
  externals: [nodeExternals()],
  output: {
    filename: 'js/serverRenderer.js',
    libraryTarget: 'commonjs2',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },
  module: {
    rules: [
        {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: 'babel-loader',
          },
          {
            test: /\.(scss|css)$/,
            include: /src/,
            use: [
                {
                    loader: 'css-loader',
                    options: {modules: { exportOnlyLocals: true}}
                },
                { loader: 'sass-loader'}
            ]
        },
            {
                test: /\.png$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ],
            },
    ],
  },
  
};

