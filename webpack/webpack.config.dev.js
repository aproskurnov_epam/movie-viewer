const { merge, CustomizeRule, mergeWithRules } = require('webpack-merge');
const common = require('./webpack.config.common');
const path = require('path');

const mergedConfig = merge(common, {
  mode: 'development',
  devtool: "eval-source-map",
});

const styleLoaderMergingRules = {
  module: {
    rules:{
      test: CustomizeRule.Match,
      use: CustomizeRule.Prepend 
    }
  }
};

const styleLoaderRules = {
  module: {
    rules:[
      {
        test: /\.(scss|css)$/,
        use: [{loader:'style-loader'}]
      }
    ]
  }
}

module.exports = mergeWithRules(styleLoaderMergingRules)(mergedConfig, styleLoaderRules);