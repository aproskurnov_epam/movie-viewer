const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const isDevMod = process.env.NODE_ENV === 'development';

module.exports = {
    mode: process.env.NODE_ENV,

  name: 'client',
  target: 'web',

  entry: [
    isDevMod && 'webpack-hot-middleware/client',
    './src/client.jsx',
  ].filter(Boolean),

  
  
    output: {
      filename: 'js/[name].js',
      path: path.resolve('./dist'),
    },

  resolve: {
    extensions: ['.js', '.jsx'],
  },

  module: {
    rules: [
        {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: 'babel-loader',
          },
      {
        test: /\.(scss|css)$/,
        include: /src/,
        use: [
          isDevMod ? 'style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
            },
          },
          { loader: 'sass-loader'}
        ],
      },
      {
        test: /\.png$/i,
        use: [
            {
                loader: 'file-loader',
            },
        ],
    },
    ],
  },

  plugins: [
    !isDevMod && new CleanWebpackPlugin('./dist', { root: path.resolve(__dirname, '../') }),
    isDevMod && new webpack.HotModuleReplacementPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css',
    }),
  ].filter(Boolean),
}